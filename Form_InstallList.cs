﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Microsoft.Win32;
using System.IO;
using System.Reflection;


namespace SetupProgram
{
    public partial class Form_InstallList : Form
    {
        public Form_InstallList()
        {
            InitializeComponent();
            Text = CData.title;
        }

        private void Form_InstallList_Load(object sender, EventArgs e)
        {
            //레지스트리에 저장된 NX 설치경로를 dic에 저장.
            List<string> temp = new List<string>();
            temp.AddRange(GetNXVersionOfLocalMachine("SOFTWARE\\Siemens", "UGII_BASE_DIR"));
            temp.AddRange(GetNXVersionOfLocalMachine("SOFTWARE\\Unigraphics Solutions\\NX", "UGII_BASE_DIR"));
            temp = temp.Distinct().ToList();
            CData.dic.Clear();
            foreach (string str in temp)
            {
                string dir = Path.GetFileName(str);
                if (dir.Length == 0)
                    dir = Path.GetFileName(Path.GetDirectoryName(str));
                dir = Util.RemoveStringSpaces(dir);
                CData.dic.Add(dir, str);
            }

            //List<string> enable = SetDescendingList(CData.dic.Keys.ToList());
            //List<string> toBe = SetDescendingList(CData.toBeInstall.ToList());

            lbEnableInstall.Items.AddRange(CData.dic.Keys.ToArray());
            lbToBeInstall.Items.AddRange(CData.toBeInstall.ToArray());
            btnBack.Visible = CData.isNewEnvironment;
        }

        private void btnBack_Click(object sender, EventArgs e)
        {
            CData.curForm = FormID.INSTALLFOLDER_0;
            this.Close();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            CData.curForm = FormID.EXIT;
            this.Close();
        }

        private void btnNext_Click(object sender, EventArgs e)
        {
            if (lbToBeInstall.Items.Count == 0)
            {
                MessageBox.Show("설치 목록을 선택하세요");
                return;
            }

            //Check Install File Exist - 설치파일 존재하는지 확인
            DirectoryInfo dirInfo = new DirectoryInfo(CData.exeDirPath);
            DirectoryInfo[] directories = dirInfo.GetDirectories();
            string name = "";
            if (CData.cbSpaceCAD)
                name = "CAD";
            else if (CData.cbSpaceCAE)
                name = "CAE";

            bool dirFound = false;
            foreach (DirectoryInfo dinfo in directories)
            {
                if (dinfo.Name == name)
                {
                    dirFound = true;
                }
            }

            if (!dirFound)
            {
                MessageBox.Show(name + " 설치 폴더가 없습니다.");
                return;
            }

            //설치할 버전 List 저장.
            CData.toBeInstall.Clear();
            foreach (var obj in lbToBeInstall.Items)
            {
                CData.toBeInstall.Add(obj.ToString());
            }

            //설치하기로 NX Version들 경로저장.
            CData.selDic.Clear();
            foreach (string str in CData.toBeInstall)
            {
                CData.selDic.Add(str, CData.dic[str]);
            }

            //라이센스가 이미 설치되어있는지 확인
            bool isLicenseExist = false;
            if (Directory.Exists(CData.licenseFolder))
            {
                DirectoryInfo diInfo = new DirectoryInfo(CData.licenseFolder);
                FileInfo[] fiInfos = diInfo.GetFiles();
                if (fiInfos.Length > 0)
                    isLicenseExist = true;
            }

            if (!isLicenseExist)//라이센스 폼으로 이동
                CData.curForm = FormID.LICENSE_2;
            else//설치진행
            {
                if (Form_License.Install())
                {
                    MessageBox.Show("설치 완료\n" + CData.error, "Message");
                    CData.curForm = FormID.EXIT;
                    this.Close();
                }
            }
            this.Close();
        }

        private void btnRight_Click(object sender, EventArgs e)
        {
            //선택된 버전 우측으로 이동.
            List<string> temp = new List<string>();
            System.Windows.Forms.ListBox.SelectedObjectCollection colObj = lbEnableInstall.SelectedItems;
            foreach (var obj in colObj)
            {
                if (!lbToBeInstall.Items.Contains(obj.ToString()))
                {
                    temp.Add(obj.ToString());
                }
            }

            //List<string> toBe = SetDescendingList(temp);
            lbToBeInstall.Items.AddRange(temp.ToArray());
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            //선택한 항목 List에서 삭제
            List<string> temp = new List<string>();
            System.Windows.Forms.ListBox.SelectedObjectCollection colObj = lbToBeInstall.SelectedItems;
            foreach (object obj in colObj)
            {
                temp.Add(obj.ToString());
            }
            foreach (string str in temp)
            {
                lbToBeInstall.Items.Remove(str);
            }
        }


        public static List<string> GetNXVersionOfLocalMachine(string path, string searchName)
        {
            List<string> results = new List<string>();
            RegistryKey reg = Registry.LocalMachine;
            reg = reg.OpenSubKey(path);
            if (reg != null)
            {
                string[] subFolder = reg.GetSubKeyNames();
                foreach (string str in subFolder)
                {
                    RegistryKey regSub = reg.OpenSubKey(str);
                    if (regSub != null)
                    {
                        object obj = regSub.GetValue(searchName);
                        if (obj != null)
                            results.Add(obj.ToString());
                    }
                }
            }

            return results;
        }

        public static List<string> SetDescendingList(List<string> list)
        {   //내림차순
            string[] strs = new string[list.Count];
            strs = list.ToArray();

            string temp = "";
            for (int i = 0; i < strs.Length; i++)
            {
                for (int j = i + 1; j < strs.Length; j++)
                {
                    double str1 = Convert.ToDouble(strs[i].Substring(2));
                    double str2 = Convert.ToDouble(strs[j].Substring(2));
                    if (str1 < str2)
                    {
                        temp = strs[i].ToString();
                        strs[i] = strs[j];
                        strs[j] = temp;
                    }
                }
            }
            return strs.ToList();
        }

        private void btnAll_Click(object sender, EventArgs e)
        {
            //모든 버전 우측으로 이동.
            List<string> temp = new List<string>();
            System.Windows.Forms.ListBox.ObjectCollection colObj = lbEnableInstall.Items;
            foreach (var obj in colObj)
            {
                temp.Add(obj.ToString());
            }
            //List<string> toBe = SetDescendingList(temp);
            lbToBeInstall.Items.Clear();
            lbToBeInstall.Items.AddRange(temp.ToArray());
        }

        public static void DeleteDirectory(string path)
        {
            foreach (string directory in Directory.GetDirectories(path))
            {
                DeleteDirectory(directory);
            }

            try
            {
                Directory.Delete(path, true);
            }
            catch (IOException)
            {
                Directory.Delete(path, true);
            }
            catch (UnauthorizedAccessException)
            {
                Directory.Delete(path, true);
            }
        }

    }
}


