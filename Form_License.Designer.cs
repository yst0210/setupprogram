﻿namespace SetupProgram
{
    partial class Form_License
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.btnInstall = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnBack = new System.Windows.Forms.Button();
            this.rbtnLicNo = new System.Windows.Forms.RadioButton();
            this.gbLicNo = new System.Windows.Forms.GroupBox();
            this.label4 = new System.Windows.Forms.Label();
            this.tbHostID = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.tbClientName = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.tbCompanyName = new System.Windows.Forms.TextBox();
            this.gbLicYes = new System.Windows.Forms.GroupBox();
            this.txtLicensePath = new System.Windows.Forms.TextBox();
            this.btnSelectPath = new System.Windows.Forms.Button();
            this.rbtnLicYes = new System.Windows.Forms.RadioButton();
            this.gbLicNo.SuspendLayout();
            this.gbLicYes.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(14, 14);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(85, 15);
            this.label1.TabIndex = 15;
            this.label1.Text = "Set License";
            // 
            // btnInstall
            // 
            this.btnInstall.Location = new System.Drawing.Point(375, 335);
            this.btnInstall.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnInstall.Name = "btnInstall";
            this.btnInstall.Size = new System.Drawing.Size(86, 29);
            this.btnInstall.TabIndex = 17;
            this.btnInstall.Text = "Install";
            this.btnInstall.UseVisualStyleBackColor = true;
            this.btnInstall.Click += new System.EventHandler(this.btnInstall_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(467, 335);
            this.btnCancel.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(86, 29);
            this.btnCancel.TabIndex = 18;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnBack
            // 
            this.btnBack.Location = new System.Drawing.Point(283, 335);
            this.btnBack.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnBack.Name = "btnBack";
            this.btnBack.Size = new System.Drawing.Size(86, 29);
            this.btnBack.TabIndex = 19;
            this.btnBack.Text = "Back";
            this.btnBack.UseVisualStyleBackColor = true;
            this.btnBack.Click += new System.EventHandler(this.btnBack_Click);
            // 
            // rbtnLicNo
            // 
            this.rbtnLicNo.AutoCheck = false;
            this.rbtnLicNo.AutoSize = true;
            this.rbtnLicNo.Location = new System.Drawing.Point(11, 0);
            this.rbtnLicNo.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.rbtnLicNo.Name = "rbtnLicNo";
            this.rbtnLicNo.Size = new System.Drawing.Size(191, 19);
            this.rbtnLicNo.TabIndex = 22;
            this.rbtnLicNo.TabStop = true;
            this.rbtnLicNo.Text = "License File 이 없습니다";
            this.rbtnLicNo.UseVisualStyleBackColor = true;
            this.rbtnLicNo.Click += new System.EventHandler(this.rbtnLicNo_Click);
            // 
            // gbLicNo
            // 
            this.gbLicNo.Controls.Add(this.label4);
            this.gbLicNo.Controls.Add(this.tbHostID);
            this.gbLicNo.Controls.Add(this.label3);
            this.gbLicNo.Controls.Add(this.tbClientName);
            this.gbLicNo.Controls.Add(this.label2);
            this.gbLicNo.Controls.Add(this.tbCompanyName);
            this.gbLicNo.Controls.Add(this.rbtnLicNo);
            this.gbLicNo.Location = new System.Drawing.Point(11, 148);
            this.gbLicNo.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.gbLicNo.Name = "gbLicNo";
            this.gbLicNo.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.gbLicNo.Size = new System.Drawing.Size(538, 166);
            this.gbLicNo.TabIndex = 23;
            this.gbLicNo.TabStop = false;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(61, 101);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(52, 15);
            this.label4.TabIndex = 28;
            this.label4.Text = "식별자";
            // 
            // tbHostID
            // 
            this.tbHostID.Location = new System.Drawing.Point(179, 96);
            this.tbHostID.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tbHostID.MaxLength = 20;
            this.tbHostID.Name = "tbHostID";
            this.tbHostID.Size = new System.Drawing.Size(282, 25);
            this.tbHostID.TabIndex = 27;
            this.tbHostID.TextChanged += new System.EventHandler(this.tbHostID_TextChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(61, 70);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(52, 15);
            this.label3.TabIndex = 26;
            this.label3.Text = "담당자";
            // 
            // tbClientName
            // 
            this.tbClientName.Location = new System.Drawing.Point(179, 65);
            this.tbClientName.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tbClientName.MaxLength = 20;
            this.tbClientName.Name = "tbClientName";
            this.tbClientName.Size = new System.Drawing.Size(282, 25);
            this.tbClientName.TabIndex = 25;
            this.tbClientName.TextChanged += new System.EventHandler(this.tbClientName_TextChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(61, 39);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(109, 15);
            this.label2.TabIndex = 24;
            this.label2.Text = "회사이름(필수)";
            // 
            // tbCompanyName
            // 
            this.tbCompanyName.Location = new System.Drawing.Point(179, 34);
            this.tbCompanyName.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tbCompanyName.MaxLength = 20;
            this.tbCompanyName.Name = "tbCompanyName";
            this.tbCompanyName.Size = new System.Drawing.Size(282, 25);
            this.tbCompanyName.TabIndex = 23;
            this.tbCompanyName.TextChanged += new System.EventHandler(this.tbCompanyName_TextChanged);
            // 
            // gbLicYes
            // 
            this.gbLicYes.Controls.Add(this.txtLicensePath);
            this.gbLicYes.Controls.Add(this.btnSelectPath);
            this.gbLicYes.Controls.Add(this.rbtnLicYes);
            this.gbLicYes.Location = new System.Drawing.Point(11, 65);
            this.gbLicYes.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.gbLicYes.Name = "gbLicYes";
            this.gbLicYes.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.gbLicYes.Size = new System.Drawing.Size(538, 76);
            this.gbLicYes.TabIndex = 24;
            this.gbLicYes.TabStop = false;
            // 
            // txtLicensePath
            // 
            this.txtLicensePath.Location = new System.Drawing.Point(11, 32);
            this.txtLicensePath.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtLicensePath.Name = "txtLicensePath";
            this.txtLicensePath.ReadOnly = true;
            this.txtLicensePath.Size = new System.Drawing.Size(409, 25);
            this.txtLicensePath.TabIndex = 24;
            // 
            // btnSelectPath
            // 
            this.btnSelectPath.Location = new System.Drawing.Point(434, 28);
            this.btnSelectPath.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnSelectPath.Name = "btnSelectPath";
            this.btnSelectPath.Size = new System.Drawing.Size(91, 29);
            this.btnSelectPath.TabIndex = 23;
            this.btnSelectPath.Text = "찾아보기...";
            this.btnSelectPath.UseVisualStyleBackColor = true;
            this.btnSelectPath.Click += new System.EventHandler(this.btnSelectPath_Click);
            // 
            // rbtnLicYes
            // 
            this.rbtnLicYes.AutoCheck = false;
            this.rbtnLicYes.AutoSize = true;
            this.rbtnLicYes.Checked = true;
            this.rbtnLicYes.Location = new System.Drawing.Point(7, 0);
            this.rbtnLicYes.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.rbtnLicYes.Name = "rbtnLicYes";
            this.rbtnLicYes.Size = new System.Drawing.Size(196, 19);
            this.rbtnLicYes.TabIndex = 22;
            this.rbtnLicYes.TabStop = true;
            this.rbtnLicYes.Text = "License File 이 있습니다.";
            this.rbtnLicYes.UseVisualStyleBackColor = true;
            this.rbtnLicYes.Click += new System.EventHandler(this.rbtnLicYes_Click);
            // 
            // Form_License
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(565, 386);
            this.Controls.Add(this.gbLicYes);
            this.Controls.Add(this.gbLicNo);
            this.Controls.Add(this.btnBack);
            this.Controls.Add(this.btnInstall);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Name = "Form_License";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Space CAD / Space CAE 설치";
            this.Load += new System.EventHandler(this.Form_License_Load);
            this.gbLicNo.ResumeLayout(false);
            this.gbLicNo.PerformLayout();
            this.gbLicYes.ResumeLayout(false);
            this.gbLicYes.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnInstall;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Button btnBack;
        private System.Windows.Forms.RadioButton rbtnLicNo;
        private System.Windows.Forms.GroupBox gbLicNo;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox tbHostID;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox tbClientName;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox tbCompanyName;
        private System.Windows.Forms.GroupBox gbLicYes;
        private System.Windows.Forms.TextBox txtLicensePath;
        private System.Windows.Forms.Button btnSelectPath;
        private System.Windows.Forms.RadioButton rbtnLicYes;
    }
}