﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Reflection;
using System.Diagnostics;
using IWshRuntimeLibrary;
using Microsoft.Win32;

namespace SetupProgram
{
    public class CADFunction
    {
        /// <summary>
        /// 디렉토리 복사
        /// </summary>
        /// <param name="strSource"></param>
        /// <param name="strDestination"></param>
        public static void CopyDirectory(string strSource, string strDestination)
        {
            //디렉토리 없으면 디렉토리 생성
            if (!Directory.Exists(strDestination))
            {
                Directory.CreateDirectory(strDestination);
            }

            //모든 파일 복사
            DirectoryInfo dirInfo = new DirectoryInfo(strSource);
            FileInfo[] files = dirInfo.GetFiles();
            foreach (FileInfo tempfile in files)
            {
                try
                {
                    Console.WriteLine("Copy File : " + tempfile.Name);
                    tempfile.CopyTo(Path.Combine(strDestination, tempfile.Name), true);
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
            }

            //모든 폴더 복사
            DirectoryInfo[] directories = dirInfo.GetDirectories();
            foreach (DirectoryInfo tempdir in directories)
            {
                CopyDirectory(Path.Combine(strSource, tempdir.Name), Path.Combine(strDestination, tempdir.Name));
            }
        }

        /// <summary>
        /// NX 버전 정보 가져오기
        /// </summary>
        /// <param name="ugii_base_dir"></param>
        /// <returns></returns>
        public static string GetNXVersion(string ugii_base_dir)
        {
            string result = "";

            try
            {
                string ugii_root_dir = Path.Combine(ugii_base_dir, "UGII");
                //NX 버전 가져오기
                ProcessStartInfo cmd = new ProcessStartInfo();
                Process process = new Process();
                cmd.FileName = Path.Combine(ugii_root_dir, "env_print.exe");
                cmd.Arguments = "-n";
                cmd.WorkingDirectory = ugii_root_dir;
                cmd.CreateNoWindow = true;
                cmd.UseShellExecute = false;
                cmd.RedirectStandardOutput = true;
                cmd.RedirectStandardInput = false;
                cmd.RedirectStandardError = false;
                process.StartInfo = cmd;
                process.Start();
                result = process.StandardOutput.ReadToEnd();
                process.StandardOutput.Close();

                //프로세스 종료
                process.WaitForExit();
                process.Close();
            }
            catch (Exception)
            {

            }

            return result;
        }


        /// <summary>
        ///  프로그램 설치
        /// </summary>
        /// <param name="exeFilePath"></param>
        /// <param name="ugii_root_dir"></param>
        /// <param name="programName"></param>
        /// <param name="ssaide_install_dir"></param>
        /// <returns></returns>
        public static bool InstallExternalProgram(string exeFilePath, string ugii_root_dir, string programName, string installSSAIDEDir)
        {
            try
            {
                //복사파일 경로 = UGII 밑에 폴더명 + exe
                string destPath = Path.Combine(ugii_root_dir, programName + ".exe");
                System.IO.File.Copy(exeFilePath, destPath, true);

                //NXOpen.SF.dll, SSLBase.dll 복사
                string dir=Path.GetDirectoryName(exeFilePath);
                System.IO.File.Copy(Path.Combine(dir,"NXOpen.SF.dll"), destPath, true);
                System.IO.File.Copy(Path.Combine(dir,"SSLBase.dll"), destPath, true);

                //EXE 파일 밑에 Config 파일이 있다면 그것도 복사
                string configFilePath = exeFilePath + ".config";
                if (System.IO.File.Exists(configFilePath))
                {
                    string destConfigPath = Path.Combine(ugii_root_dir, programName + ".exe.config");
                    System.IO.File.Copy(configFilePath, destConfigPath, true);
                }

                //라이선스 경로 SSAIDE Utility로 지정
                string licensePath = Path.Combine(installSSAIDEDir, "License\\License.dat");
                string registryPath = @"Software\SpaceSolution\" + programName;
                SetRegistry(registryPath, "LicensePath", licensePath);

                //바탕화면에 바로가기 만들기
                CreateShortCut(ugii_root_dir, destPath);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        /// <summary>
        /// 바탕화면에 바로가기 만들기
        /// </summary>
        /// <param name="originalPath"></param>
        /// <param name="exeFilePath"></param>
        private static bool CreateShortCut(string originalPath, string exeFilePath)
        {
            try
            {
                //바탕화면 폴더
                string deskTopDir = Environment.GetFolderPath(Environment.SpecialFolder.DesktopDirectory);

                //프로그램 명
                string programName = System.IO.Path.GetFileNameWithoutExtension(exeFilePath);

                //개체 생성
                WshShell wshShellClass = new WshShell();

                IWshRuntimeLibrary.IWshShortcut objShortcut;

                // 바로가기를 저장할 경로를 지정한다.
                objShortcut = (IWshRuntimeLibrary.IWshShortcut)wshShellClass.CreateShortcut(deskTopDir + "\\" + programName + ".lnk");

                // 바로가기에 프로그램의 경로를 지정한다.
                objShortcut.TargetPath = originalPath + "\\" + programName + ".exe";

                // 시작 위치를 지정한다.
                objShortcut.WorkingDirectory = originalPath;

                // 바로가기의 description을 지정한다.
                objShortcut.Description = programName + ".exe 바로가기";

                // 바로가기 아이콘을 지정한다.
                objShortcut.IconLocation = objShortcut.TargetPath;

                // 바로가기를 저장한다.
                objShortcut.Save();

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        //RegistryKey ---------------------
        public static RegistryKey RegistryCheckAndCreate(string registryPath)
        {
            RegistryKey registryKey = Registry.CurrentUser.OpenSubKey(registryPath, true);

            if (registryKey == null)
            {
                string[] splitPath = registryPath.Split('\\');

                if (splitPath.Length > 0)
                {
                    registryKey = Registry.CurrentUser.OpenSubKey(splitPath[0], true);

                    if (registryKey != null)
                    {
                        for (int i = 1; i < splitPath.Length; i++)
                        {
                            if (registryKey.OpenSubKey(splitPath[i], true) == null)
                            {
                                registryKey.CreateSubKey(splitPath[i]);
                            }

                            registryKey = registryKey.OpenSubKey(splitPath[i], true);
                        }
                    }
                }
            }

            return registryKey;
        }

        /// <summary>
        /// 레지스트리 값 설정
        /// </summary>
        /// <param name="registryPath"></param>
        /// <param name="key"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        public static bool SetRegistry(string registryPath, string key, string value)
        {
            RegistryKey registryKey = RegistryCheckAndCreate(registryPath);

            if (registryKey != null)
            {
                registryKey.SetValue(key, value);
            }

            return true;
        }

        /// <summary>
        /// 레지스트리 값 가져오기
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public static string GetRegistry(string registryPath, string key)
        {
            RegistryKey registryKey = RegistryCheckAndCreate(registryPath);
            string value = null;

            if (registryKey.GetValue(key, value) != null)
            {
                value = registryKey.GetValue(key, value).ToString();
            }

            return value;
        }


    }
}
