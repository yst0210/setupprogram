﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using Microsoft.VisualBasic;
using System.Security.AccessControl;
using System.Security.Principal;
using System.Text.RegularExpressions;
using System.Diagnostics;
using System.Net;


namespace SetupProgram
{
    public enum FormID {
        //SELECTINSTALL_0, 
        INSTALLFOLDER_0, INSTALLLIST_1,  LICENSE_2, EXIT }
    public class CData
    {
        public static string title = "SpaceUtil 설치";
        public static bool cbSpaceCAD = true;
        public static bool cbSpaceCAE = true;
        public static bool cbDWGExport = true;

        public static bool rBtnLicYes = true;
        public static bool rBtnLicNo = false;
        public static bool check = true;

        public static Dictionary<string, string> dic = new Dictionary<string, string>();
        public static Dictionary<string, string> selDic = new Dictionary<string, string>();
        public static string selLicenseFilePath = "";
        public static string InstallDirName = "SSAIDE";

        public static string parentFolder = "C:\\";
        public static string InstallFolder = parentFolder + InstallDirName;
        public static string licenseFolder = InstallFolder + "\\License";

        public static string envSSAIDEVar = "UGII_SSAIDE_DIR";
        public static bool isNewEnvironment;
        public static string exeDirPath = Application.StartupPath;
        public static string hostID = Dns.GetHostName();
        public static string error = "";

        public static List<string> toBeInstall = new List<string>();

        public static FormID curForm = FormID.INSTALLFOLDER_0;

        public static void SetTitle()
        {
            if (cbSpaceCAD && !cbSpaceCAE) title = "Space CAD 설치";
            else if (!cbSpaceCAD && cbSpaceCAE) title = "Space CAE 설치";
            else if (cbSpaceCAD && cbSpaceCAE) title = "Space CAD / CAE 설치";            
        }
   
    }
}
