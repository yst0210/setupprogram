﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Reflection;
using System.Diagnostics;
using IWshRuntimeLibrary;
using Microsoft.Win32;
namespace SetupProgram
{
    public class SetupDWGExport
    {
        public static void DWGExportInstall(string installSSAIDEDir, string ugii_base_dir, List<string> nxVer)
        {
            string EXTERNAL_DIR = "External";

            //External 프로그램 설치
            string codeBase1 = Assembly.GetExecutingAssembly().CodeBase;
            UriBuilder uri1 = new UriBuilder(codeBase1);
            string dllPath1 = Uri.UnescapeDataString(uri1.Path);
            string directory1 = Path.GetDirectoryName(dllPath1);
            directory1 = Path.Combine(directory1, EXTERNAL_DIR);
            if (Directory.Exists(directory1))
            {
                //Console.WriteLine("External 프로그램 설치를 시작합니다.");

                bool is32bit = false;
                string ugii_root_dir = Path.Combine(ugii_base_dir, "UGII");
                string dll_path = Path.Combine(ugii_root_dir, "msvcp71.dll");
                if (System.IO.File.Exists(dll_path))
                {
                    //특정 파일이 존재하면 32bit
                    is32bit = true;
                }
                else
                {
                    //없을 경우는 64bit
                    is32bit = false;
                }

                DirectoryInfo externalDirInfo = new DirectoryInfo(directory1);
                //모든 디렉토리 정보 가져오기(외부 프로그램 개수 만큼 존재)
                DirectoryInfo[] externalProgrameDirs = externalDirInfo.GetDirectories();
                for (int i = 0; i < externalProgrameDirs.Length; i++)
                {
                    FileInfo[] files = externalProgrameDirs[i].GetFiles();
                    //복사할 파일 경로
                    string copyFilePath = null;
                    //안에 있는 x84와 x64 용 파일
                    string x84FilePath = null;
                    string x64FilePath = null;
                    string x64_12VerFilePath = null;
                    string x64_1872VerFilePath = null;
                    string x64_1899VerFilePath = null;
                    string x64_1926VerFilePath = null;
                    for (int k = 0; k < files.Length; k++)
                    {
                        //실행 파일만 가져오기 
                        string extension = files[k].Extension;
                        if (extension.ToLower() == ".exe")
                        {
                            string fileName = Path.GetFileNameWithoutExtension(files[k].FullName);
                            if (fileName.EndsWith("_32"))
                            {
                                //32bit 용 파일
                                x84FilePath = files[k].FullName;
                            }
                            else if (fileName.EndsWith("_64"))
                            {
                                //64bit 용 파일
                                x64FilePath = files[k].FullName;
                            }
                            else if (fileName.EndsWith("_64_12"))
                            {
                                //64bit 12 Ver 용 파일
                                x64_12VerFilePath = files[k].FullName;
                            }
                            else if (fileName.EndsWith("_64_1872"))
                            {
                                //64bit 1872 Ver 용 파일
                                x64_1872VerFilePath = files[k].FullName;
                            }
                            else if (fileName.EndsWith("_64_1899"))
                            {
                                //64bit 1899 Ver 용 파일
                                x64_1899VerFilePath = files[k].FullName;
                            }
                            else if (fileName.EndsWith("_64_1926"))
                            {
                                //64bit 1926 Ver 용 파일
                                x64_1926VerFilePath = files[k].FullName;
                            }
                        }
                    }

                    //32bit 버전
                    if (is32bit && x84FilePath != null)
                    {
                        copyFilePath = x84FilePath;
                    }
                    //64bit 버전
                    else if (!is32bit && x64FilePath != null)
                    {
                        copyFilePath = x64FilePath;
                    }

                    //NX 12 버전              
                    if (nxVer[1].Length > 0)
                    {
                        double versionNumber = Convert.ToDouble(nxVer[0]);
                        string[] productVersionInfo = nxVer[1].Split(' ');
                        if (productVersionInfo.Length > 0 && double.TryParse(productVersionInfo[productVersionInfo.Length - 1], out versionNumber) && versionNumber >= 11)
                        {
                            if (versionNumber >= 12)
                            {
                                copyFilePath = x64_12VerFilePath;
                                //NX1847 버전 보다 상위일때 처리
                                if (versionNumber >= 1926)
                                {
                                    copyFilePath = x64_1926VerFilePath;
                                }
                                else if (versionNumber >= 1899)
                                {
                                    copyFilePath = x64_1899VerFilePath;
                                }
                                else if (versionNumber >= 1872)
                                {
                                    copyFilePath = x64_1872VerFilePath;
                                }
                            }
                        }
                    }

                    //파일 복사
                    if (copyFilePath != null)
                    {
                        //NX 11버전 대응 소스
                        string destPath = ugii_root_dir;
                        if (nxVer[1].Length > 0)
                        {
                            double versionNumber = Convert.ToDouble(nxVer[0]);
                            string[] productVersionInfo = nxVer[1].Split(' ');
                            if (productVersionInfo.Length > 0 && double.TryParse(productVersionInfo[productVersionInfo.Length - 1], out versionNumber) && versionNumber >= 11)
                            {
                                if (versionNumber >= 11)
                                {
                                    string binPath = Path.Combine(ugii_base_dir, "NXBIN");
                                    if (System.IO.Directory.Exists(binPath))
                                    {
                                        destPath = binPath;
                                    }
                                }
                            }
                        }

                        //External 프로그램 설치
                        if (!CADFunction.InstallExternalProgram(copyFilePath, destPath, externalProgrameDirs[i].Name, installSSAIDEDir))
                            MessageBox.Show(externalProgrameDirs[i].Name + " 프로그램 설치중 문제가 발생하였습니다.");
                    }
                }
            }

        }
    }
}
