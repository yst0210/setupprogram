﻿namespace SetupProgram
{
    partial class Form_InstallList
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form_InstallList));
            this.btnBack = new System.Windows.Forms.Button();
            this.btnNext = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.gbNXList = new System.Windows.Forms.GroupBox();
            this.btnAll = new System.Windows.Forms.Button();
            this.btnRight = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.lbToBeInstall = new System.Windows.Forms.ListBox();
            this.lbEnableInstall = new System.Windows.Forms.ListBox();
            this.btnDelete = new System.Windows.Forms.Button();
            this.gbNXList.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnBack
            // 
            this.btnBack.Location = new System.Drawing.Point(227, 262);
            this.btnBack.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnBack.Name = "btnBack";
            this.btnBack.Size = new System.Drawing.Size(86, 29);
            this.btnBack.TabIndex = 27;
            this.btnBack.Text = "Back";
            this.btnBack.UseVisualStyleBackColor = true;
            this.btnBack.Visible = false;
            this.btnBack.Click += new System.EventHandler(this.btnBack_Click);
            // 
            // btnNext
            // 
            this.btnNext.Location = new System.Drawing.Point(319, 262);
            this.btnNext.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnNext.Name = "btnNext";
            this.btnNext.Size = new System.Drawing.Size(86, 29);
            this.btnNext.TabIndex = 25;
            this.btnNext.Text = "Next";
            this.btnNext.UseVisualStyleBackColor = true;
            this.btnNext.Click += new System.EventHandler(this.btnNext_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(411, 262);
            this.btnCancel.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(86, 29);
            this.btnCancel.TabIndex = 26;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // gbNXList
            // 
            this.gbNXList.Controls.Add(this.btnAll);
            this.gbNXList.Controls.Add(this.btnRight);
            this.gbNXList.Controls.Add(this.label1);
            this.gbNXList.Controls.Add(this.label2);
            this.gbNXList.Controls.Add(this.lbToBeInstall);
            this.gbNXList.Controls.Add(this.lbEnableInstall);
            this.gbNXList.Controls.Add(this.btnDelete);
            this.gbNXList.Location = new System.Drawing.Point(12, 12);
            this.gbNXList.Name = "gbNXList";
            this.gbNXList.Size = new System.Drawing.Size(485, 243);
            this.gbNXList.TabIndex = 24;
            this.gbNXList.TabStop = false;
            this.gbNXList.Text = "설치할 NX Version";
            // 
            // btnAll
            // 
            this.btnAll.Image = global::SetupProgram.Properties.Resources.AllButton;
            this.btnAll.Location = new System.Drawing.Point(222, 71);
            this.btnAll.Name = "btnAll";
            this.btnAll.Size = new System.Drawing.Size(45, 45);
            this.btnAll.TabIndex = 16;
            this.btnAll.UseVisualStyleBackColor = true;
            this.btnAll.Click += new System.EventHandler(this.btnAll_Click);
            // 
            // btnRight
            // 
            this.btnRight.Image = ((System.Drawing.Image)(resources.GetObject("btnRight.Image")));
            this.btnRight.Location = new System.Drawing.Point(222, 122);
            this.btnRight.Name = "btnRight";
            this.btnRight.Size = new System.Drawing.Size(45, 45);
            this.btnRight.TabIndex = 15;
            this.btnRight.UseVisualStyleBackColor = true;
            this.btnRight.Click += new System.EventHandler(this.btnRight_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(19, 31);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(163, 15);
            this.label1.TabIndex = 13;
            this.label1.Text = "설치 가능한 NX Version";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(280, 31);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(72, 15);
            this.label2.TabIndex = 14;
            this.label2.Text = "설치 목록";
            // 
            // lbToBeInstall
            // 
            this.lbToBeInstall.FormattingEnabled = true;
            this.lbToBeInstall.ItemHeight = 15;
            this.lbToBeInstall.Location = new System.Drawing.Point(283, 61);
            this.lbToBeInstall.Name = "lbToBeInstall";
            this.lbToBeInstall.SelectionMode = System.Windows.Forms.SelectionMode.MultiExtended;
            this.lbToBeInstall.Size = new System.Drawing.Size(187, 169);
            this.lbToBeInstall.TabIndex = 12;
            // 
            // lbEnableInstall
            // 
            this.lbEnableInstall.FormattingEnabled = true;
            this.lbEnableInstall.ItemHeight = 15;
            this.lbEnableInstall.Location = new System.Drawing.Point(22, 61);
            this.lbEnableInstall.Name = "lbEnableInstall";
            this.lbEnableInstall.SelectionMode = System.Windows.Forms.SelectionMode.MultiExtended;
            this.lbEnableInstall.Size = new System.Drawing.Size(187, 169);
            this.lbEnableInstall.TabIndex = 9;
            // 
            // btnDelete
            // 
            this.btnDelete.Image = global::SetupProgram.Properties.Resources.xButton;
            this.btnDelete.Location = new System.Drawing.Point(222, 173);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(45, 45);
            this.btnDelete.TabIndex = 11;
            this.btnDelete.UseVisualStyleBackColor = true;
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // Form_InstallList
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(510, 304);
            this.Controls.Add(this.btnBack);
            this.Controls.Add(this.btnNext);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.gbNXList);
            this.Name = "Form_InstallList";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Space CAD / Space CAE 설치";
            this.Load += new System.EventHandler(this.Form_InstallList_Load);
            this.gbNXList.ResumeLayout(false);
            this.gbNXList.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnBack;
        private System.Windows.Forms.Button btnNext;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.GroupBox gbNXList;
        private System.Windows.Forms.Button btnRight;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ListBox lbToBeInstall;
        private System.Windows.Forms.ListBox lbEnableInstall;
        private System.Windows.Forms.Button btnDelete;
        private System.Windows.Forms.Button btnAll;
    }
}