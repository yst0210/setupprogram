﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Windows;
using System.Windows.Forms;
using System.Xml;
using System.Diagnostics;
using System.Reflection;
using System.Security.Permissions;
using Microsoft.Win32;
using System.Text.RegularExpressions;
namespace SetupProgram
{
    public class Util
    {

        //string-------------------------
        public static string RemoveStringSpaces(string str)
        {
            string newStr = Regex.Replace(str, @"\s", "");
            return newStr;
        }

        public static string OpenFolderDlg()
        {
            string folderPath = "";
            FolderBrowserDialog folder = new FolderBrowserDialog();
            if (folder.ShowDialog() == DialogResult.OK)
                folderPath = folder.SelectedPath;
            return folderPath;
        }

        public static string OpenFileDlg(string InitialfolderPath, string filter)
        {
            string dirFullPath = "";
            OpenFileDialog open = new OpenFileDialog();
            open.InitialDirectory = InitialfolderPath;
            open.Filter = filter;
            open.Title = "Open File";
            if (open.ShowDialog() == DialogResult.OK)
            {
                dirFullPath = open.FileName;
            }
            return dirFullPath;
        }
        public static void DeleteDirectory(string target_dir)
        {
            string[] files = Directory.GetFiles(target_dir);
            string[] dirs = Directory.GetDirectories(target_dir);
            foreach (string file in files)
            {
                File.SetAttributes(file, FileAttributes.Normal);
                File.Delete(file);
            }
            foreach (string dir in dirs)
            {
                DeleteDirectory(dir);
            }
            Directory.Delete(target_dir, false);
        }

        public static bool CreateFolder(string folderFullPath, out DirectoryInfo di)
        {
            di = new DirectoryInfo(folderFullPath);
            if (!di.Exists)
                di.Create();
            else
                return false;
            return true;
        }

        public static bool CreateFolder(string dirPath, string name, out DirectoryInfo di)
        {
            di = new DirectoryInfo(Path.Combine(dirPath, name));
            if (!di.Exists)
                di.Create();
            else
                return false;
            return true;
        }

        public static bool IsFileExist(string path)
        {
            FileInfo fi = new FileInfo(path);
            return fi.Exists;
        }
        public static bool CopyFile(string originFileFullPath, string destFileFullPath)
        {//파일이름이 동일해야 복사가능.            
            if (Path.GetFileName(originFileFullPath) == Path.GetFileName(destFileFullPath))
                File.Copy(originFileFullPath, string.Format(destFileFullPath), true);
            else
                return false;

            return true;
        }
    }
}
