﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Reflection;
using System.Diagnostics;
using IWshRuntimeLibrary;
using Microsoft.Win32;


namespace SetupProgram
{
    public class Setup
    {
        public static bool EditCustomDir(string installFolder, string ugiiBaseDir, List<string> nxVer, string type)
        {
            string nxPath1 = ugiiBaseDir + "\\UGII\\menus\\custom_dirs.dat";
            string nxPath2 = ugiiBaseDir + "\\UGII\\menus\\custom_dirs.dat_default";
            string nxPath3 = ugiiBaseDir + "\\UGII\\menus\\ug_custom_dirs.dat";
            string simPath1 = ugiiBaseDir + "\\SIMULATION\\simcenter\\menus\\custom_dirs.dat";
            string simPath2 = ugiiBaseDir + "\\SIMULATION\\simcenter\\menus\\custom_dirs.dat_default";
            string simPath3 = ugiiBaseDir + "\\SIMULATION\\simcenter\\menus\\ug_custom_dirs.dat";
            string custom_app_path = "";
            string custom_dir_path = "";

            List<string> nxPath = new List<string>(new string[] { nxPath1, nxPath2, nxPath3 });
            List<string> simPath = new List<string>(new string[] { simPath1, simPath2, simPath3 });

            for (int i = 0; i < 3; i++)
            {
                if (System.IO.File.Exists(nxPath[i]))
                {
                    if (type == "CAD")
                        GetCADCustomPath(CData.envSSAIDEVar, nxPath[i], nxVer, out custom_app_path, out custom_dir_path);
                    else if (type == "CAE")
                        GetCAECustomPath(CData.envSSAIDEVar, nxPath[i], nxVer, out custom_app_path, out custom_dir_path);

                    if (!WriteCustomDir(nxPath[i], custom_app_path, custom_dir_path))
                        return false;
                    if (System.IO.File.Exists(simPath[i]))
                        WriteCustomDir(simPath[i], custom_app_path, custom_dir_path);
                }
                else
                {
                    MessageBox.Show(nxPath[i] + "설치파일이 존재하지 않습니다.");
                    return false;
                }
            }
            return true;
        }

        public static void GetCADCustomPath(string SSAIDE_DIR_ENVIRMENT_NAME, string customDirPath, List<string> nxVer
            , out string custom_app_path, out string custom_dir_path)
        {
            System.IO.File.SetAttributes(customDirPath, FileAttributes.Normal);

            //NX 프로그램에 있는 custom_dirs.dat 파일 읽음, 없을 경우만 SSAIDE 환경변수 경로 지정
            custom_app_path = "${" + SSAIDE_DIR_ENVIRMENT_NAME + "}\\";
            custom_dir_path = "${" + SSAIDE_DIR_ENVIRMENT_NAME + "}\\CAD\\NX110";

            //NX 버전 체크
            //NX 버전 생성 8.5, 9 CustomDir 수정은 됬는데 NX 리본 탭에 뜨지 않음.                             
            double versionNumber = Convert.ToDouble(nxVer[0]);
            if (versionNumber >= 12)
            {
                //12버전 이상인 경우 StartUp 경로 변경
                custom_dir_path = "${" + SSAIDE_DIR_ENVIRMENT_NAME + "}\\CAD\\NX120";
                //NX1847 버전 보다 상위일때 처리
                if (versionNumber >= 1926)
                {
                    custom_dir_path = "${" + SSAIDE_DIR_ENVIRMENT_NAME + "}\\CAD\\NX1926";
                }
                else if (versionNumber >= 1899)
                {
                    custom_dir_path = "${" + SSAIDE_DIR_ENVIRMENT_NAME + "}\\CAD\\NX1899";
                }
                else if (versionNumber >= 1872)
                {
                    custom_dir_path = "${" + SSAIDE_DIR_ENVIRMENT_NAME + "}\\CAD\\NX1872";
                }
            }
            else if (versionNumber == 8)
            {
                if (nxVer[1].Length > 1 && nxVer[2] == "0")
                {//8버전인 경우는 StartUp 경로 변경
                    custom_dir_path = "${" + SSAIDE_DIR_ENVIRMENT_NAME + "}\\CAD\\NX080";
                }
            }
            else if (versionNumber <= 7)
            {
                //8버전 아래인 경우는 StartUp 경로 변경
                custom_dir_path = "${" + SSAIDE_DIR_ENVIRMENT_NAME + "}\\CAD\\NX075";
            }

        }

        public static void GetCAECustomPath(string SSAIDE_DIR_ENVIRMENT_NAME, string customDirPath, List<string> nxVer
          , out string custom_app_path, out string custom_dir_path)
        {
            System.IO.File.SetAttributes(customDirPath, FileAttributes.Normal);

            //NX 프로그램에 있는 custom_dirs.dat 파일 읽음, 없을 경우만 SSAIDE 환경변수 경로 지정
            custom_app_path = "${" + SSAIDE_DIR_ENVIRMENT_NAME + "}\\";
            custom_dir_path = "${" + SSAIDE_DIR_ENVIRMENT_NAME + "}\\CAE\\NX110";

            //NX 버전 체크
            //NX 버전 생성 8.5, 9 CustomDir 수정은 됬는데 NX 리본 탭에 뜨지 않음.                             
            double versionNumber = Convert.ToDouble(nxVer[0]);
            //1953버전 이상인 경우 StartUp 경로 변경
            if (versionNumber >= 12)
                custom_dir_path = "${" + SSAIDE_DIR_ENVIRMENT_NAME + "}\\CAE\\NX120";
            if (versionNumber >= 1847)
                custom_dir_path = "${" + SSAIDE_DIR_ENVIRMENT_NAME + "}\\CAE\\NX1847";
            if (versionNumber >= 1872)
                custom_dir_path = "${" + SSAIDE_DIR_ENVIRMENT_NAME + "}\\CAE\\NX1872";
            if (versionNumber >= 1899)
                custom_dir_path = "${" + SSAIDE_DIR_ENVIRMENT_NAME + "}\\CAE\\NX1899";
            if (versionNumber >= 1926)
                custom_dir_path = "${" + SSAIDE_DIR_ENVIRMENT_NAME + "}\\CAE\\NX1926";
            if (versionNumber >= 1953)
                custom_dir_path = "${" + SSAIDE_DIR_ENVIRMENT_NAME + "}\\CAE\\NX1953";
        }

        public static bool WriteCustomDir(string custom_dirs_dat, string custom_app_path, string custom_dir_path)
        {
            //custom_dirs_dat - custom_dir.dat 파일 위치
            //custom_app_path - NX 내부 프로그램 application 위치
            //custom_dir_path - NX 내부 프로그램 startup 위치

            string[] allLines = System.IO.File.ReadAllLines(custom_dirs_dat);
            if (!allLines.Contains(custom_dir_path))
            {
                try
                {
                    System.IO.File.SetAttributes(custom_dirs_dat, FileAttributes.Normal);
                    System.IO.File.AppendAllText(custom_dirs_dat, custom_dir_path);
                }
                catch (Exception ex)
                {
                    Console.WriteLine("NX 내부 실행용 프로그램 설치 중 문제가 발생하였습니다.\n예외 : " + ex.Message);
                    return false;
                }
            }
            return true;
        }

        public static bool DeleteCustomDir(string custom_dirs_dat, string word)
        {
            //custom_dirs_dat - custom_dir.dat 파일 위치
            //custom_app_path - NX 내부 프로그램 application 위치
            //custom_dir_path - NX 내부 프로그램 startup 위치
            if (!System.IO.File.Exists(custom_dirs_dat))
                return false;
            string[] allLines = System.IO.File.ReadAllLines(custom_dirs_dat);
            List<string> writeLines = new List<string>();
            if (!allLines.Contains(word))
            {
                try
                {
                    //word를 포함한 Line, 공란 제거
                    System.IO.File.SetAttributes(custom_dirs_dat, FileAttributes.Normal);                    
                    foreach (string str in allLines)
                    {
                        if (!str.Contains(word) && !string.IsNullOrWhiteSpace(str))
                            writeLines.Add(str);
                    }
                    System.IO.File.WriteAllLines(custom_dirs_dat, writeLines);
                }
                catch (Exception ex)
                {
                    Console.WriteLine("CustomDir 삭제 중 문제가 발생하였습니다.\n예외 : " + ex.Message);
                    return false;
                }
            }
            return true;
        }

    }
}
