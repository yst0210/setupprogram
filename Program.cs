﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Reflection;
using System.IO;

namespace SetupProgram
{
    public static class Program
    {
        /// <summary>
        /// 해당 응용 프로그램의 주 진입점입니다.
        /// </summary>
        [STAThread]
        public static void Main(bool isCAD, bool isCAE)
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            CData.cbSpaceCAD = isCAD;
            CData.cbSpaceCAE = isCAE;
            CData.SetTitle();

            CData.InstallFolder = Environment.GetEnvironmentVariable(CData.envSSAIDEVar, EnvironmentVariableTarget.Machine);
            if (CData.InstallFolder == null || CData.InstallFolder == "" || !Directory.Exists(CData.InstallFolder))
            {
                CData.parentFolder = "C:\\";
                CData.InstallFolder = CData.parentFolder + CData.InstallDirName;
                CData.curForm = FormID.INSTALLFOLDER_0;
                CData.isNewEnvironment = true;
            }
            else
            {
                CData.parentFolder = "";
                CData.curForm = FormID.INSTALLLIST_1;
                CData.isNewEnvironment = false;
            }
            CData.licenseFolder = CData.InstallFolder + "\\License";

            while (CData.curForm!= FormID.EXIT)
            {
                switch (CData.curForm)
                {
                    //case FormID.SELECTINSTALL_0:
                    //    Application.Run(new Form_SelectInstall());
                    //    break;
                    case FormID.INSTALLFOLDER_0:
                        Application.Run(new Form_InstallFolder());
                        break;
                    case FormID.INSTALLLIST_1:
                        Application.Run(new Form_InstallList());
                        break;
                    case FormID.LICENSE_2:
                        Application.Run(new Form_License());
                        break;
                }
            }
        }
    }
}
