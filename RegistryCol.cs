﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Reflection;
using System.Diagnostics;
using IWshRuntimeLibrary;
using Microsoft.Win32;


namespace SetupProgram
{
    public class RegistryCol
    {
        public static void WriteRegistry(string path, string name, string val)
        {//path : "Software\\Microsoft\\Fusion", name : key Name , value : 1
            //키 생성하기
            RegistryKey regKey = Registry.LocalMachine.CreateSubKey(path, RegistryKeyPermissionCheck.ReadWriteSubTree);

            //값 저장하기
            regKey.SetValue(name, val, RegistryValueKind.DWord);
        }

        public static string ReadRegistry(string path, string regVal)
        {//path : "Software\\Microsoft\\Fusion"
            RegistryKey reg = Registry.LocalMachine;
            reg = reg.OpenSubKey(path, true);

            // TEST못 열면
            if (reg == null)
                return "";

            // 값 있으면
            if (null != reg.GetValue(regVal))
            {
                return Convert.ToString(reg.GetValue(regVal));
            }
            else
            {
                // 값 없으면
                return "";
            }
        }

        public static bool DeleteRegistry(string path, string val)
        {// path : "Software\\TEST"
            if (ReadRegistry(path, val) != "")
                Registry.LocalMachine.CreateSubKey(path).DeleteSubKey(val);
            else
                return false;

            return true;
        }


    }
}
