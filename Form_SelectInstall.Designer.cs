﻿namespace SetupProgram
{
    partial class Form_SelectInstall
    {
        /// <summary>
        /// 필수 디자이너 변수입니다.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 사용 중인 모든 리소스를 정리합니다.
        /// </summary>
        /// <param name="disposing">관리되는 리소스를 삭제해야 하면 true이고, 그렇지 않으면 false입니다.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form 디자이너에서 생성한 코드

        /// <summary>
        /// 디자이너 지원에 필요한 메서드입니다. 
        /// 이 메서드의 내용을 코드 편집기로 수정하지 마세요.
        /// </summary>
        private void InitializeComponent()
        {
            this.gbSelInstall = new System.Windows.Forms.GroupBox();
            this.cbSpaceCAE = new System.Windows.Forms.CheckBox();
            this.cbSpaceCAD = new System.Windows.Forms.CheckBox();
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnNext = new System.Windows.Forms.Button();
            this.gbSelInstall.SuspendLayout();
            this.SuspendLayout();
            // 
            // gbSelInstall
            // 
            this.gbSelInstall.Controls.Add(this.cbSpaceCAE);
            this.gbSelInstall.Controls.Add(this.cbSpaceCAD);
            this.gbSelInstall.Location = new System.Drawing.Point(133, 29);
            this.gbSelInstall.Name = "gbSelInstall";
            this.gbSelInstall.Size = new System.Drawing.Size(200, 120);
            this.gbSelInstall.TabIndex = 2;
            this.gbSelInstall.TabStop = false;
            this.gbSelInstall.Text = "설치목록";
            // 
            // cbSpaceCAE
            // 
            this.cbSpaceCAE.AutoSize = true;
            this.cbSpaceCAE.Checked = true;
            this.cbSpaceCAE.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbSpaceCAE.Location = new System.Drawing.Point(43, 75);
            this.cbSpaceCAE.Name = "cbSpaceCAE";
            this.cbSpaceCAE.Size = new System.Drawing.Size(104, 19);
            this.cbSpaceCAE.TabIndex = 2;
            this.cbSpaceCAE.Text = "Space CAE";
            this.cbSpaceCAE.UseVisualStyleBackColor = true;
            this.cbSpaceCAE.CheckedChanged += new System.EventHandler(this.cbSpaceCAE_CheckedChanged);
            // 
            // cbSpaceCAD
            // 
            this.cbSpaceCAD.AutoSize = true;
            this.cbSpaceCAD.Checked = true;
            this.cbSpaceCAD.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbSpaceCAD.Location = new System.Drawing.Point(43, 39);
            this.cbSpaceCAD.Name = "cbSpaceCAD";
            this.cbSpaceCAD.Size = new System.Drawing.Size(105, 19);
            this.cbSpaceCAD.TabIndex = 1;
            this.cbSpaceCAD.Text = "Space CAD";
            this.cbSpaceCAD.UseVisualStyleBackColor = true;
            this.cbSpaceCAD.CheckedChanged += new System.EventHandler(this.cbSpaceCAD_CheckedChanged);
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(348, 170);
            this.btnCancel.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(86, 29);
            this.btnCancel.TabIndex = 6;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnNext
            // 
            this.btnNext.Location = new System.Drawing.Point(247, 170);
            this.btnNext.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnNext.Name = "btnNext";
            this.btnNext.Size = new System.Drawing.Size(86, 29);
            this.btnNext.TabIndex = 5;
            this.btnNext.Text = "Next";
            this.btnNext.UseVisualStyleBackColor = true;
            this.btnNext.Click += new System.EventHandler(this.btnNext_Click);
            // 
            // Form_SelectInstall
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(463, 213);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnNext);
            this.Controls.Add(this.gbSelInstall);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "Form_SelectInstall";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Space CAD / Space CAE 설치";
            this.Load += new System.EventHandler(this.Form_SelectInstall_Load);
            this.gbSelInstall.ResumeLayout(false);
            this.gbSelInstall.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox gbSelInstall;
        private System.Windows.Forms.CheckBox cbSpaceCAE;
        private System.Windows.Forms.CheckBox cbSpaceCAD;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Button btnNext;
    }
}

