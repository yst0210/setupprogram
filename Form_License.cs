﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Reflection;
using System.Diagnostics;
using Microsoft.Win32;
using System.Runtime.CompilerServices;
using System.Net;
using System.Net.NetworkInformation;
using System.Management;
using System.Text.RegularExpressions;
namespace SetupProgram
{
    public partial class Form_License : Form
    {
        public Form_License()
        {
            InitializeComponent();
            this.rbtnLicYes.Click += rbtnLicYes_Click;
            this.rbtnLicNo.Click += rbtnLicNo_Click;
            Text = CData.title;
        }
        public void rbtnLicYes_Click(object sender, EventArgs e)
        {
            CData.rBtnLicYes = true;
            CData.rBtnLicNo = false;
            RadioButtonControl();
        }
        public void rbtnLicNo_Click(object sender, EventArgs e)
        {
            CData.rBtnLicYes = false;
            CData.rBtnLicNo = true;
            RadioButtonControl();
        }

        public bool CheckLic(out string error)
        {
            bool check = true;
            //int check = 0; //0 = Install, 1 = Error, 2 = return
            error = "";

            if (CData.rBtnLicYes)
            {
                if (txtLicensePath.TextLength == 0)
                {
                    error = "라이센스가 선택되지 않았습니다.";
                    return false;
                }
                if (!File.Exists(txtLicensePath.Text))
                {
                    error = "라이센스 파일이 존재하지 않습니다.";
                    return false;
                }
            }
            else if (CData.rBtnLicNo)
            {
                if (tbCompanyName.Text.Length == 0 || tbClientName.Text.Length == 0 || tbHostID.Text.Length == 0)
                {
                    if (MessageBox.Show(new Form() { TopMost = true }, "라이센스 발급을 위한 값이 입력되지 않았습니다." +
                        " \n계속하시겠습니까?", "Warning", MessageBoxButtons.YesNo) == DialogResult.No)
                    {
                        return false;
                    }
                }

                if (check)
                    if (!CreateUserInfo(out error))
                        return false;
            }

            return true;
        }

        public void InitializeButton(bool pBarValue, bool btnValue)
        {
            btnInstall.Enabled = btnValue;
            btnBack.Enabled = btnValue;
            btnSelectPath.Enabled = btnValue;
        }


        private void btnBack_Click(object sender, EventArgs e)
        {
            CData.curForm = FormID.INSTALLLIST_1;
            this.Close();
        }

        private void btnInstall_Click(object sender, EventArgs e)
        {
            InitializeButton(true, false);
            string error = "";

            if (CData.isNewEnvironment)
            {
                if (!Directory.Exists(CData.parentFolder))
                {
                    Directory.CreateDirectory(CData.parentFolder);
                    if (!Directory.Exists(CData.parentFolder))
                    {
                        MessageBox.Show(CData.parentFolder + " 폴더 생성에 실패하였습니다. 다른 경로를 선택해 주세요.");
                        return;
                    }
                }

                if (!Directory.Exists(CData.InstallFolder))
                {
                    Directory.CreateDirectory(CData.InstallFolder);
                    if (!Directory.Exists(CData.InstallFolder))
                    {
                        MessageBox.Show(CData.InstallFolder + " 폴더 생성에 실패하였습니다. 다른 경로를 선택해 주세요.");
                        return;
                    }
                }
            }

            if (!Directory.Exists(CData.licenseFolder))
            {
                Directory.CreateDirectory(CData.licenseFolder);
            }

            if (CheckLic(out error))
            {
                //설치된 경로 - 환경변수로 수정.
                //Environment.SetEnvironmentVariable(CData.envSSAIDEVar, CData.InstallSSAIDEDirPath, EnvironmentVariableTarget.User);
                Environment.SetEnvironmentVariable(CData.envSSAIDEVar, CData.InstallFolder, EnvironmentVariableTarget.Machine);

                //설치된 경로 환경변수로 지정
                //string registryPath = @"SYSTEM\CurrentControlSet\Control\Sessionmanager\Environment";
                //SetRegistry(registryPath, CData.envSSAIDEVar, CData.InstallSSAIDEDirPath);

                ////라이센스 파일을 SpaceUtil\\License 경로로 이동
                if (CData.rBtnLicYes)
                {
                    if (CData.selLicenseFilePath.Length > 0)
                    {
                        //Util.CopyFile(selLicenseFilePath, Path.Combine(CData.InstallLicDirPath, Path.GetFileName(selLicenseFilePath)));
                        Util.CopyFile(CData.selLicenseFilePath, CData.licenseFolder + "\\" + Path.GetFileName(CData.selLicenseFilePath));
                    }
                }
                if (Install())
                {
                    MessageBox.Show("설치 완료\n"+CData.error, "Message");
                    CData.curForm = FormID.EXIT;
                    this.Close();
                }
            }
            if (error.Length > 0)
                MessageBox.Show(error);
            InitializeButton(false, true);
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            CData.curForm = FormID.EXIT;
            this.Close();
        }

        private void btnSelectPath_Click(object sender, EventArgs e)
        {
            txtLicensePath.Text = Util.OpenFileDlg("C:", "SpaceSolution License File(*.txt,*.dat)|*.txt;*.dat");
            CData.selLicenseFilePath = txtLicensePath.Text;
        }

        public static bool GetNXVersion(string ugii_base_dir, out List<string> version)
        {
            string nxVersion = CADFunction.GetNXVersion(ugii_base_dir);
            string[] nxVersionInfo = nxVersion.Split('.');
            double versionNumber = 0;
            version = new List<string>();

            if (nxVersionInfo.Length > 0)
            {
                string[] productVersionInfo = nxVersionInfo[0].Split(' ');
                if (productVersionInfo.Length > 0 && double.TryParse(productVersionInfo[productVersionInfo.Length - 1], out versionNumber))
                { }
                else
                    return false;
            }
            version.Add(versionNumber.ToString());
            version.AddRange(nxVersionInfo);

            return true;
        }

        private void Form_License_Load(object sender, EventArgs e)
        {
            tbHostID.Text = CData.hostID;
            RadioButtonControl();
        }

        public static bool Install()
        {
            List<string> nxVersion = new List<string>();
            
            //Copy Folder     
            DirectoryInfo dirInfo = new DirectoryInfo(CData.exeDirPath);
            string name = "";
            if (CData.cbSpaceCAD)
                name = "CAD";
            else if (CData.cbSpaceCAE)
                name = "CAE";

            string emptyFolderList = "";
            foreach (KeyValuePair<string, string> val in CData.selDic)
            {
                if (!File.Exists(val.Value+ "\\UGII\\menus\\custom_dirs.dat"))
                    emptyFolderList += val.Value + "\n";
            }

            if (emptyFolderList.Length > 0)
            {
                MessageBox.Show(emptyFolderList + "폴더가 존재하지 않습니다.", "Message");
                return false;
            }

            CADFunction.CopyDirectory(dirInfo.FullName+"\\"+name, CData.InstallFolder + "\\" + name);      

            //선택한 버전에 있는 Custom_dirs.dat 파일에 기존에 입력된 설치경로 삭제
            List<string> customDirList = new List<string>();
            foreach (KeyValuePair<string, string> val in CData.selDic)
            {
                customDirList.Add(val.Value + "\\UGII\\menus\\custom_dirs.dat");
                customDirList.Add(val.Value + "\\UGII\\menus\\custom_dirs.dat_default");
                customDirList.Add(val.Value + "\\UGII\\menus\\ug_custom_dirs.dat");
                customDirList.Add(val.Value + "\\SIMULATION\\simcenter\\menus\\custom_dirs.dat");
                customDirList.Add(val.Value + "\\SIMULATION\\simcenter\\menus\\custom_dirs.dat_default");
                customDirList.Add(val.Value + "\\SIMULATION\\simcenter\\menus\\ug_custom_dirs.dat");
                foreach (string str in customDirList)
                {
                    Setup.DeleteCustomDir(str, "${UGII_SSAIDE_DIR}\\" + name);
                    Setup.DeleteCustomDir(str, "${UGII_SSAIDE_DIR}\\API");
                }
            }

            //Custom_Dirs.dat에 선택한 NX 버전 경로 입력
            foreach (KeyValuePair<string, string> val in CData.selDic)
            {
                //NX Version 추출
                if (GetNXVersion(val.Value, out nxVersion))
                {
                    if (CData.cbSpaceCAD)
                    {
                        SetupDWGExport.DWGExportInstall(CData.InstallFolder, val.Value, nxVersion);
                        if (!Setup.EditCustomDir(CData.InstallFolder, val.Value, nxVersion, "CAD"))
                            return false;
                    }
                    if (CData.cbSpaceCAE)
                    {
                        //NX Curdom_dir.dat 수정
                        if (!Setup.EditCustomDir(CData.InstallFolder, val.Value, nxVersion, "CAE"))
                            return false;
                    }
                }
                else
                    CData.error += val.Value +"은 NX Vesion을 읽을 수 없어서 설치할 수 없습니다. \n";
            }

            return true;
        }



        public void RadioButtonControl()
        {
            if (CData.rBtnLicYes)
            {
                rbtnLicYes.Checked = true;
                rbtnLicNo.Checked = false;
                btnSelectPath.Enabled = true;
                txtLicensePath.Enabled = true;

                label2.Enabled = false;
                label3.Enabled = false;
                label4.Enabled = false;
                tbClientName.Enabled = false;
                tbCompanyName.Enabled = false;
                tbHostID.Enabled = false;
            }

            if (CData.rBtnLicNo)
            {
                rbtnLicYes.Checked = false;
                rbtnLicNo.Checked = true;
                btnSelectPath.Enabled = false;
                txtLicensePath.Enabled = false;

                label2.Enabled = true;
                label3.Enabled = true;
                label4.Enabled = true;
                tbClientName.Enabled = true;
                tbCompanyName.Enabled = true;
                tbHostID.Enabled = true;
            }
        }

        struct NetworkInfo
        {
            public string macAddress;
            public string description;
        }

        public bool CreateUserInfo(out string error)
        {
            List<string> userInfo = new List<string>();
            error = "";
            //if (Regex.IsMatch(tbCompanyName.Text + tbClientName.Text, @"[^a-zA-Z0-9 ]"))
            //{
            //    error = "입력값에 특수문자는 입력할 수 없습니다.";
            //    return false;
            //}                    

            string companyStr = tbCompanyName.Text;
            string clientStr = tbClientName.Text;
            string idStr = tbHostID.Text;

            char[] nullStr = new char[1];
            nullStr[0] = ' ';

            companyStr = companyStr.Trim(nullStr);
            clientStr = clientStr.Trim(nullStr);
            idStr = idStr.Trim(nullStr);

            if (companyStr.Length == 0) companyStr = "NoName";
            if (clientStr.Length == 0) clientStr = "NoName";
            if (idStr.Length == 0) idStr = "NoName";

            userInfo.Add(companyStr);
            userInfo.Add(clientStr);
            userInfo.Add(idStr);

            NetworkInfo[] networkInfos = GetMacAddressAndDescription();
            userInfo.Add(networkInfos.Length.ToString());
            foreach (NetworkInfo address_description in networkInfos)
            {
                if (address_description.macAddress.Length > 11)
                {
                    userInfo.Add(address_description.macAddress + " " + address_description.description);
                }
            }

            string filePath = Path.Combine(CData.exeDirPath, "UserInfo_" + companyStr + "_" + clientStr + "_" + idStr + ".txt");
            CreateFile(filePath);
            File.WriteAllLines(filePath, userInfo);
            //userInfo.Add("OS Name is : " + GetOSFriendlyName());
            return true;
        }

        /// <summary>
        /// Mac Address를 가져오는 함수
        /// </summary>
        /// <returns></returns>
        private static NetworkInfo[] GetMacAddressAndDescription()
        {
            List<NetworkInfo> phsical_address_and_description_str = new List<NetworkInfo>();
            NetworkInterface[] netinterfaces = NetworkInterface.GetAllNetworkInterfaces();
            foreach (NetworkInterface netinterface in netinterfaces)
            {
                if (netinterface.NetworkInterfaceType == NetworkInterfaceType.Ethernet || netinterface.NetworkInterfaceType == NetworkInterfaceType.Wireless80211)
                {
                    PhysicalAddress phsical_address = netinterface.GetPhysicalAddress();
                    NetworkInfo networkInfo;
                    networkInfo.macAddress = phsical_address.ToString();
                    networkInfo.description = netinterface.Description;
                    phsical_address_and_description_str.Add(networkInfo);
                }
            }
            return phsical_address_and_description_str.ToArray();
        }

        /// <summary>
        /// OS 정보를 가져오는 함수
        /// </summary>
        /// <returns></returns>
        public static string GetOSFriendlyName()
        {
            string result = "";
            ManagementObjectSearcher searcher = new ManagementObjectSearcher("SELECT Caption FROM Win32_OperatingSystem");
            foreach (ManagementObject os in searcher.Get())
            {
                result = os["Caption"].ToString();
                break;
            }
            return result;
        }

        public static void CreateFile(string path)
        {
            bool check = true;
            int num = 1;
            while (check)
            {
                if (!File.Exists(path))
                {
                    FileStream fs = File.Create(path);
                    fs.Close();
                    check = false;
                }
                path = Path.Combine(Path.GetDirectoryName(path), Path.GetFileNameWithoutExtension(path) + "(" + num + ")" + ".txt");
                num++;
            }


        }

        private void tbCompanyName_TextChanged(object sender, EventArgs e)
        {
        }

        private void tbClientName_TextChanged(object sender, EventArgs e)
        {
        }

        private void tbHostID_TextChanged(object sender, EventArgs e)
        {
            CData.hostID = tbHostID.Text;
        }
        public static bool SetRegistry(string registryPath, string key, string value)
        {
            RegistryKey registryKey = RegistryCheckAndCreate(registryPath);

            if (registryKey != null)
            {
                registryKey.SetValue(key, value);
            }

            return true;
        }

        public static RegistryKey RegistryCheckAndCreate(string registryPath)
        {
            RegistryKey registryKey = Registry.LocalMachine.OpenSubKey(registryPath, true);

            if (registryKey == null)
            {
                string[] splitPath = registryPath.Split('\\');

                if (splitPath.Length > 0)
                {
                    registryKey = Registry.LocalMachine.OpenSubKey(splitPath[0], true);

                    if (registryKey != null)
                    {
                        for (int i = 1; i < splitPath.Length; i++)
                        {
                            if (registryKey.OpenSubKey(splitPath[i], true) == null)
                            {
                                registryKey.CreateSubKey(splitPath[i]);
                            }

                            registryKey = registryKey.OpenSubKey(splitPath[i], true);
                        }
                    }
                }
            }

            return registryKey;
        }
    }
}
