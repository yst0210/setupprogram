﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Reflection;

namespace SetupProgram
{
    public partial class Form_SelectInstall : Form
    {
        public Form_SelectInstall()
        {
            InitializeComponent();
        }

        private void btnNext_Click(object sender, EventArgs e)
        {
            CData.curForm = FormID.INSTALLLIST_1;
            this.Close();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            CData.curForm = FormID.EXIT;
            this.Close();
        }

        private void cbSpaceCAD_CheckedChanged(object sender, EventArgs e)
        {
            CData.cbSpaceCAD = cbSpaceCAD.Checked;
        }

        private void cbSpaceCAE_CheckedChanged(object sender, EventArgs e)
        {
            CData.cbSpaceCAE = cbSpaceCAE.Checked;
        }

        private void cbDWGExport_CheckedChanged(object sender, EventArgs e)
        {
        }

        private void Form_SelectInstall_Load(object sender, EventArgs e)
        {
            cbSpaceCAE.Checked = CData.cbSpaceCAE;
            cbSpaceCAD.Checked = CData.cbSpaceCAD;
        }

    }
}
