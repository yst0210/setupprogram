﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace SetupProgram
{
    public partial class Form_InstallFolder : Form
    {
        public Form_InstallFolder()
        {
            InitializeComponent();
            Text = CData.title;
        }

        private void Form_InstallFolder_Load(object sender, EventArgs e)
        {
            txtParentFolder.Text = CData.parentFolder;
            txtInstallFolder.Text = CData.InstallFolder;
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            CData.curForm = FormID.EXIT;
            this.Close();
        }

        private void btnSelectPath_Click(object sender, EventArgs e)
        {
            string folderPath = Util.OpenFolderDlg();
            if (folderPath.Length > 0)
            {
                if (folderPath.Last() != '\\') folderPath += '\\';
                CData.parentFolder = txtParentFolder.Text = folderPath;
                CData.InstallFolder = txtInstallFolder.Text = folderPath + CData.InstallDirName;
                CData.licenseFolder = CData.InstallFolder + "\\License";
            }
        }

        private void btnNext_Click(object sender, EventArgs e)
        {
            if (!Directory.Exists(CData.parentFolder))
            {
                Directory.CreateDirectory(CData.parentFolder);
                if (!Directory.Exists(CData.parentFolder))
                {
                    MessageBox.Show(CData.parentFolder + " 폴더 생성에 실패하였습니다. 다른 경로를 선택해 주세요.");
                    return;
                }
            }
            if (!Directory.Exists(CData.InstallFolder))
            {
                Directory.CreateDirectory(CData.InstallFolder);
                if (!Directory.Exists(CData.InstallFolder))
                {
                    MessageBox.Show(CData.InstallFolder + " 폴더 생성에 실패하였습니다. 다른 경로를 선택해 주세요.");
                    return;
                }
            }
            
            CData.curForm = FormID.INSTALLLIST_1;
            this.Close();
        }
    }
}
